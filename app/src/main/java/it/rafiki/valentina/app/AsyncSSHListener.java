package it.rafiki.valentina.app;

import net.schmizz.sshj.connection.channel.direct.Session;

/**
 * Created by Administrator on 16/03/14.
 */
public interface AsyncSSHListener {
    public void onClientConnected(Session.Shell shellInitialized);
}

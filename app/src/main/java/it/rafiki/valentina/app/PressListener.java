package it.rafiki.valentina.app;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import net.schmizz.sshj.common.StreamCopier;
import net.schmizz.sshj.connection.ConnectionException;
import net.schmizz.sshj.connection.channel.direct.Session.Shell;
import net.schmizz.sshj.transport.TransportException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 15/03/14.
 */
public class PressListener implements OnTouchListener {
    final boolean[] first_time;
    final Shell shell;
    final String comando;

    public PressListener(Shell arg1, boolean[] arg2, String arg3){
        shell=arg1;
        first_time=arg2;
        comando=arg3+"\n";
    }
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (!first_time[0]) {
            try {
            InputStream stream = new ByteArrayInputStream(comando.getBytes("UTF-8"));
            new StreamCopier(stream, shell.getOutputStream())
                    .bufSize(shell.getRemoteMaxPacketSize())
                    .copy();
            } catch (ConnectionException e) {
                e.printStackTrace();
            } catch (TransportException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            first_time[0] = true;
        }
        return false;
    }

}

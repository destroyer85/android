package it.rafiki.valentina.app;

import android.os.AsyncTask;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.connection.channel.direct.Session.Shell;
import net.schmizz.sshj.transport.verification.HostKeyVerifier;

import java.io.IOException;
import java.security.PublicKey;

/**
 * Created by Administrator on 16/03/14.
 */
public class AsyncSSHClient extends AsyncTask<String[], Integer,Shell> {
    SSHClient ssh;
    Session session;
    Shell shell;
    AsyncSSHListener asyncSSHListener;
    String server,utente,password;
    Integer porta;

    public AsyncSSHClient(String server, Integer porta, String utente,String password,AsyncSSHListener listener){
        this.asyncSSHListener = listener;
        this.server =server;
        this.utente = utente;
        this.password =password;
        this.porta = porta;
    }

    @Override
    protected Shell doInBackground(String[]... params) {
        ssh = new SSHClient();
        ssh.addHostKeyVerifier(
                new HostKeyVerifier() {
                    public boolean verify(String arg0, int arg1, PublicKey arg2) {
                        return true;  // don't bother verifying
                    }
                }
        );
        try {
            ssh.setConnectTimeout(5000);
            ssh.connect(server, porta);
            ssh.authPassword(utente, password);
            session = ssh.startSession();
            session.allocateDefaultPTY();
            shell = session.startShell();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        return shell;
    }
    protected void onPostExecute(Shell result){
        if (result!=null) {
            asyncSSHListener.onClientConnected(result);
        }
    }
}

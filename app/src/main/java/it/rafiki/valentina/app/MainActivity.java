package it.rafiki.valentina.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.StreamCopier;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.connection.channel.direct.Session.Shell;

import java.io.IOException;


public class MainActivity extends ActionBarActivity{

    SSHClient ssh;
    Session session;
    AsyncSSHClient asyncSSHClient;
    Button btnAvanti,btnRetro,btnDestra,btnSinistra,btnPirDes,btnPriSin;
    final boolean[] first_time={false};
    String server,utente,password;
    Integer porta;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnAvanti = (Button) findViewById(R.id.btn_avanti);
        btnRetro = (Button) findViewById(R.id.btn_retro);
        btnDestra = (Button) findViewById(R.id.btn_destra);
        btnSinistra = (Button) findViewById(R.id.btn_sinistra);
        btnPirDes = (Button) findViewById(R.id.btn_prides);
        btnPriSin = (Button) findViewById(R.id.btn_pirsin);
        loadPref();

    }

    public void onToggleClicked(View view){
        // Is the toggle on?
        boolean on = ((Switch) view).isChecked();

        if (on) {

            AsyncSSHListener asyncSSHListener = new AsyncSSHListener() {
                @Override
                public void onClientConnected(Shell shellInitialized) {

                    OnTouchListener pressAvantiListener = new PressListener(shellInitialized,first_time,"who");
                    OnTouchListener pressRetroListener = new PressListener(shellInitialized,first_time,"ll");
                    OnTouchListener pressSinistraListener = new PressListener(shellInitialized,first_time,"ps");
                    OnTouchListener pressDestraListener = new PressListener(shellInitialized,first_time,"date");
                    OnTouchListener pressPirDesListener = new PressListener(shellInitialized,first_time,"lspci");
                    OnTouchListener pressPirSinListener = new PressListener(shellInitialized,first_time,"lsusb");
                    OnClickListener releaseListener = new ReleaseListener(shellInitialized,first_time,"pwd");

                    btnAvanti.setOnTouchListener(pressAvantiListener);
                    btnRetro.setOnTouchListener(pressRetroListener);
                    btnDestra.setOnTouchListener(pressDestraListener);
                    btnSinistra.setOnTouchListener(pressSinistraListener);
                    btnPirDes.setOnTouchListener(pressPirDesListener);
                    btnPriSin.setOnTouchListener(pressPirSinListener);

                    btnAvanti.setOnClickListener(releaseListener);
                    btnRetro.setOnClickListener(releaseListener);
                    btnDestra.setOnClickListener(releaseListener);
                    btnSinistra.setOnClickListener(releaseListener);
                    btnPirDes.setOnClickListener(releaseListener);
                    btnPriSin.setOnClickListener(releaseListener);

                    btnAvanti.setEnabled(true);
                    btnRetro.setEnabled(true);
                    btnDestra.setEnabled(true);
                    btnSinistra.setEnabled(true);
                    btnPirDes.setEnabled(true);
                    btnPriSin.setEnabled(true);


                    new StreamCopier(shellInitialized.getInputStream(), System.out)
                            .bufSize(shellInitialized.getLocalMaxPacketSize())
                            .spawn("stdout");

                    new StreamCopier(shellInitialized.getErrorStream(), System.err)
                            .bufSize(shellInitialized.getLocalMaxPacketSize())
                            .spawn("stderr");
                }
            };
            asyncSSHClient = new AsyncSSHClient(server,porta,utente,password,asyncSSHListener);
            asyncSSHClient.execute();

        }
        else {
            try {
                asyncSSHClient.session.close();
                asyncSSHClient.ssh.disconnect();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            finally {
                btnAvanti.setEnabled(false);
                btnRetro.setEnabled(false);
                btnDestra.setEnabled(false);
                btnSinistra.setEnabled(false);
                btnPirDes.setEnabled(false);
                btnPriSin.setEnabled(false);
                Toast.makeText(this, "Disconnesso", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult(i, 1);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        //super.onActivityResult(requestCode, resultCode, data);



        loadPref();
    }

    private void loadPref(){
        SharedPreferences mySharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        server = mySharedPreferences.getString("server", "192.168.25.1");
        porta = Integer.parseInt(mySharedPreferences.getString("porta","22"));
        utente = mySharedPreferences.getString("utente", "root");
        password = mySharedPreferences.getString("password", "olimex");

    }

}
